# Hungarian-Greek-Math keyboard hybrid

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

English:

Math+Greek unicode character mod for hunagrian keyboards

Usage: 
-Turn on CapsLock to activate the characters in bottom-left position on the attached layout.png. 
-While CapsLock is active press shift for top-left, or ALTGR for top-right characters.
-Turn off caps-lock for regular typing witth hungarian layout(extended, see layout.png).
-In case Regaular CapsLock functionality is necessary, turn on CapsLock and ScrollLock.



__________________________________________________________________________________________
Magyar:

Matek+Görög szimbólumok gyűjteménye magyar billentyűzethez

Használat:
-CapsLock bekapcsolásával aktiválahtóak a melléket layout.png fájl bal alsó sarkában lévő karakterek.
-Amíg a capsLock aktív, Shift megnyomásával a bal felső, ALTGR megnyomásával a jobb felső karakterek érhetőek el.
-Capslock kikapcsolásával a billentyűzet vissza áll magyar kiosztásra(kibővített, lásd layout.png)
-Amennyiben hagyományos CApsLock funkció használata szükséges, a CapsLock mellé kapcsoljuk be a ScrollLock-ot is.
